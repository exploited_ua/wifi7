
//= ../../node_modules/swiper/dist/js/swiper.jquery.js
//= ../../node_modules/vegas/dist/vegas.js
//= jquery.ddslick.min.js

$(document).ready(function () {
var Slider = new Swiper('.slider-block', {
    slidesPerView: '1',
    pagination: '.slider-pagination',
    paginationClickable: true,
    spaceBetween: 10,
    autoplay: 2000,
    nextButton: '.swiper-button-next',
    prevButton: '.swiper-button-prev',
    breakpoints: {
        767: {
            slidesPerView: 'auto'
        }
    }
});
var Slider1 = new Swiper('.swiper1', {
    slidesPerView: '3',
    spaceBetween: 14,
    breakpoints: {
        1200: {
            slidesPerView: '2'
        },
        767: {
            slidesPerView: '2'
        },
        550: {
            slidesPerView: 'auto'
        }

    }
});

    $(function() {
        $('body').vegas({
            slides: [
                { src: 'img/background.jpg' },
                { src: 'img/background1.jpg' },
                { src: 'img/background3.jpg' }
            ],
            transition: 'swirlLeft2',
            timer: false,
            transitionDuration: 2000
        });
    });




$('#myDropdown').ddslick({
    onSelected: function(selectedData){
    }
});
// open feedback modal
    function openFeedbackModal() {
        $('#feedback').show();
    }

    $('#btn_feedback').on('click', function (e) {
        e.preventDefault();
        openFeedbackModal();
    });

// close modal
    function closeModal(){
        $('#conditions').hide();
        $('#feedback').hide();
        $('#succsess_feedback').hide();
    }

    $('.close_modal').on('click', function () {
        closeModal();
    });

// open Succsess Feedback Modal

    function openSuccsessFeedbackModal() {
        $('#feedback').hide();
        $('#succsess_feedback').show();
    }

    $('.btn_confirm').on('click', function (e) {
        e.preventDefault();
        openSuccsessFeedbackModal();
    });

// open Terms and Conditions

    function openConditionsModal() {
        $('#conditions').show();
    }

    $("#registration-form-terms-and-conditions-text").on('click', function () {
        openConditionsModal();
    });

// open After Registration block
    function openAfterRegistration() {
        $('.after-registration').show().css('display','flex');
    }

});

